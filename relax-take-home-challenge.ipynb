{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Relax Inc. Take-Home Challenge\n",
    "By Hanna Seyoum"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Relax Inc. makes productivity and project management software that's popular with both individuals and teams.  \n",
    "\n",
    "The goal of this challenge is to first define an \"adopted user\" as a user who has logged into the product on three separate days in at least one seven-day period, and to identify which factors predict future user adoption."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The Data\n",
    "There are two datasets, users & user engagement data, both of which are csv files. I use Pandas to read the files and store them as a Pandas dataframe.\n",
    "\n",
    "### Features\n",
    "<u> users dataset </u>:  \n",
    "\n",
    "**name**: the user's name  \n",
    "**object_id**: the user's id  \n",
    "**email**: email address  \n",
    "**creation_source**: how their account was created. This takes on one of 5 values:\n",
    "    \n",
    "    PERSONAL_PROJECTS: invited to join another user's personal workspace\n",
    "    GUEST_INVITE: invited to an organization as a guest (limited permissions)\n",
    "    ORG_INVITE: invited to an organization (as a full member)\n",
    "    SIGNUP: signed up via the website\n",
    "    SIGNUP_GOOGLE_AUTH: signed up using Google Authentication (using a Google email account for their login id)\n",
    "\n",
    "**creation_time**: when they created their account  \n",
    "**last_session_creation_time**: unix timestamp of last login  \n",
    "**opted_in_to_mailing_list**: whether they have opted into receiving marketing emails  \n",
    "**enabled_for_marketing_drip**: whether they are on the regular marketing email drip  \n",
    "**org_id**: the organization (group of users) they belong to  \n",
    "**invited_by_user_id**: which user invited them to join (if applicable)\n",
    "\n",
    "<u> user_engagement dataset </u>:  \n",
    "\n",
    "row for each day that a user logged into the product.  \n",
    "**user_id**: user's id  \n",
    "**time_stamp**: time user logged into account  \n",
    "**visited**: user logged into account  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Methodology\n",
    "**i. Creating an adopted user**  \n",
    "I first work with the user engagement dataframe, `df_user_engagement`, to define an adopted user. I do so by changing the time_stamp feature to datetime and setting it as an index. I then created a for-loop that checks if each user had logged in at least three times in a span of 7 days by looking at the time_stamp feature, and I store the results in a list named labels. I then add that list to `df_users` as a feature.\n",
    "\n",
    "**ii. Identifying which factors predict future user adoption**  \n",
    "This is a **classification** problem.  \n",
    "The feature `last_session_creation_time` had around 3,177 missing values. I assumed the missing values indicate that the user had not logged in again after the account was created, so I imputed the values by replacing the missing values with the values in `creation_time`.\n",
    "\n",
    "The next step I took was applying one-hot-econding, but before doing that I dropped the `name` and `email` features because applying one hot encoding to the dataframe would result in each unique name and email having it's own column, resulting in too many columns. Since I don't think these two columns will have a significant effect on the model, I've dropped them.\n",
    "\n",
    "I then defined a `train_test` function that would take the features, target, model name, and parameters as arguements."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Models\n",
    "\n",
    "**Logistic Regression**  \n",
    "Logistic regression uses regression to do classification.  \n",
    "Unlike regression which uses Least Squares, the model uses Maximum Likelihood to fit a sigmoid-curve on the target variable distribution.  \n",
    "\n",
    "**Random Forrest**  \n",
    "A Random Forest is a reliable ensemble of multiple Decision Trees, and is used for both classification and regression problems. Here, the individual trees are built via bagging (i.e. aggregation of bootstraps) and split using fewer features. The resulting diverse forest of uncorrelated trees exhibits reduced variance; therefore, is more robust towards change in data and carries its prediction accuracy to new data.\n",
    "\n",
    "**K-Nearest Neighbors (KNN)**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "from sklearn.model_selection import train_test_split\n",
    "from sklearn.model_selection import GridSearchCV\n",
    "from sklearn.metrics import confusion_matrix, classification_report\n",
    "from sklearn.metrics import auc\n",
    "from sklearn.metrics import roc_curve\n",
    "\n",
    "from sklearn.linear_model import LogisticRegression\n",
    "from sklearn.ensemble import RandomForestClassifier\n",
    "from sklearn.neighbors import KNeighborsClassifier"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "df_users = pd.read_csv(\"takehome_users.csv\", encoding=\"ISO-8859-1\", parse_dates=[\"creation_time\"])\n",
    "df_user_engagement = pd.read_csv(\"takehome_user_engagement.csv\", encoding=\"ISO-8859-1\", parse_dates=[\"time_stamp\"], index_col=0)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**users table**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "# I noticed that the last_session_creation_time feature was a unix timestamp. They appeared to be seconds since epoch,\n",
    "# so I used the following code to convert to datetime using pandas.  \n",
    "\n",
    "df_users[\"last_session_creation_time\"] = pd.to_datetime(df_users[\"last_session_creation_time\"], unit='s')\n",
    "\n",
    "# rename object_id to user_id so that I can use it to merge with the user engagement table. \n",
    "df_users.rename(columns = {\"object_id\": \"user_id\"}, inplace=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>user_id</th>\n",
       "      <th>creation_time</th>\n",
       "      <th>name</th>\n",
       "      <th>email</th>\n",
       "      <th>creation_source</th>\n",
       "      <th>last_session_creation_time</th>\n",
       "      <th>opted_in_to_mailing_list</th>\n",
       "      <th>enabled_for_marketing_drip</th>\n",
       "      <th>org_id</th>\n",
       "      <th>invited_by_user_id</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>1</td>\n",
       "      <td>2014-04-22 03:53:30</td>\n",
       "      <td>Clausen August</td>\n",
       "      <td>AugustCClausen@yahoo.com</td>\n",
       "      <td>GUEST_INVITE</td>\n",
       "      <td>2014-04-22 03:53:30</td>\n",
       "      <td>1</td>\n",
       "      <td>0</td>\n",
       "      <td>11</td>\n",
       "      <td>10803.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>2</td>\n",
       "      <td>2013-11-15 03:45:04</td>\n",
       "      <td>Poole Matthew</td>\n",
       "      <td>MatthewPoole@gustr.com</td>\n",
       "      <td>ORG_INVITE</td>\n",
       "      <td>2014-03-31 03:45:04</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>1</td>\n",
       "      <td>316.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>3</td>\n",
       "      <td>2013-03-19 23:14:52</td>\n",
       "      <td>Bottrill Mitchell</td>\n",
       "      <td>MitchellBottrill@gustr.com</td>\n",
       "      <td>ORG_INVITE</td>\n",
       "      <td>2013-03-19 23:14:52</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>94</td>\n",
       "      <td>1525.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>3</th>\n",
       "      <td>4</td>\n",
       "      <td>2013-05-21 08:09:28</td>\n",
       "      <td>Clausen Nicklas</td>\n",
       "      <td>NicklasSClausen@yahoo.com</td>\n",
       "      <td>GUEST_INVITE</td>\n",
       "      <td>2013-05-22 08:09:28</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>1</td>\n",
       "      <td>5151.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>4</th>\n",
       "      <td>5</td>\n",
       "      <td>2013-01-17 10:14:20</td>\n",
       "      <td>Raw Grace</td>\n",
       "      <td>GraceRaw@yahoo.com</td>\n",
       "      <td>GUEST_INVITE</td>\n",
       "      <td>2013-01-22 10:14:20</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>193</td>\n",
       "      <td>5240.0</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "   user_id       creation_time               name                       email  \\\n",
       "0        1 2014-04-22 03:53:30     Clausen August    AugustCClausen@yahoo.com   \n",
       "1        2 2013-11-15 03:45:04      Poole Matthew      MatthewPoole@gustr.com   \n",
       "2        3 2013-03-19 23:14:52  Bottrill Mitchell  MitchellBottrill@gustr.com   \n",
       "3        4 2013-05-21 08:09:28    Clausen Nicklas   NicklasSClausen@yahoo.com   \n",
       "4        5 2013-01-17 10:14:20          Raw Grace          GraceRaw@yahoo.com   \n",
       "\n",
       "  creation_source last_session_creation_time  opted_in_to_mailing_list  \\\n",
       "0    GUEST_INVITE        2014-04-22 03:53:30                         1   \n",
       "1      ORG_INVITE        2014-03-31 03:45:04                         0   \n",
       "2      ORG_INVITE        2013-03-19 23:14:52                         0   \n",
       "3    GUEST_INVITE        2013-05-22 08:09:28                         0   \n",
       "4    GUEST_INVITE        2013-01-22 10:14:20                         0   \n",
       "\n",
       "   enabled_for_marketing_drip  org_id  invited_by_user_id  \n",
       "0                           0      11             10803.0  \n",
       "1                           0       1               316.0  \n",
       "2                           0      94              1525.0  \n",
       "3                           0       1              5151.0  \n",
       "4                           0     193              5240.0  "
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "df_users.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**user engagement table**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>user_id</th>\n",
       "      <th>visited</th>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>time_stamp</th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>2014-04-22 03:53:30</th>\n",
       "      <td>1</td>\n",
       "      <td>1</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2013-11-15 03:45:04</th>\n",
       "      <td>2</td>\n",
       "      <td>1</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2013-11-29 03:45:04</th>\n",
       "      <td>2</td>\n",
       "      <td>1</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2013-12-09 03:45:04</th>\n",
       "      <td>2</td>\n",
       "      <td>1</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2013-12-25 03:45:04</th>\n",
       "      <td>2</td>\n",
       "      <td>1</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "                     user_id  visited\n",
       "time_stamp                           \n",
       "2014-04-22 03:53:30        1        1\n",
       "2013-11-15 03:45:04        2        1\n",
       "2013-11-29 03:45:04        2        1\n",
       "2013-12-09 03:45:04        2        1\n",
       "2013-12-25 03:45:04        2        1"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "df_user_engagement.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<class 'pandas.core.frame.DataFrame'>\n",
      "DatetimeIndex: 207917 entries, 2014-04-22 03:53:30 to 2014-01-26 08:57:12\n",
      "Data columns (total 2 columns):\n",
      " #   Column   Non-Null Count   Dtype\n",
      "---  ------   --------------   -----\n",
      " 0   user_id  207917 non-null  int64\n",
      " 1   visited  207917 non-null  int64\n",
      "dtypes: int64(2)\n",
      "memory usage: 4.8 MB\n"
     ]
    }
   ],
   "source": [
    "df_user_engagement.info()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "df_user_engagement.sort_index(inplace=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "8823"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "unique_users = len(df_user_engagement.groupby(\"user_id\"))\n",
    "unique_users"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [],
   "source": [
    "labels = []\n",
    "\n",
    "for i in range(1,12001):\n",
    "    # 12000: length of index.\n",
    "    # we start from 1 instead of 0 because user_id starts from 1\n",
    "    user_logins = df_user_engagement.loc[df_user_engagement[\"user_id\"] == i]\n",
    "    within_7_days = user_logins.rolling('7D').count()\n",
    "    if within_7_days.loc[within_7_days[\"visited\"] >= 3].shape[0] > 0:\n",
    "        adopted = 1\n",
    "    else:\n",
    "        adopted = 0\n",
    "    labels.append(adopted)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I will be making the next few changes to a dataframe named df, which starts off as a copy of df_users."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [],
   "source": [
    "df = df_users.copy()\n",
    "df[\"adopted\"] = labels"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>user_id</th>\n",
       "      <th>creation_time</th>\n",
       "      <th>name</th>\n",
       "      <th>email</th>\n",
       "      <th>creation_source</th>\n",
       "      <th>last_session_creation_time</th>\n",
       "      <th>opted_in_to_mailing_list</th>\n",
       "      <th>enabled_for_marketing_drip</th>\n",
       "      <th>org_id</th>\n",
       "      <th>invited_by_user_id</th>\n",
       "      <th>adopted</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>1</td>\n",
       "      <td>2014-04-22 03:53:30</td>\n",
       "      <td>Clausen August</td>\n",
       "      <td>AugustCClausen@yahoo.com</td>\n",
       "      <td>GUEST_INVITE</td>\n",
       "      <td>2014-04-22 03:53:30</td>\n",
       "      <td>1</td>\n",
       "      <td>0</td>\n",
       "      <td>11</td>\n",
       "      <td>10803.0</td>\n",
       "      <td>0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>2</td>\n",
       "      <td>2013-11-15 03:45:04</td>\n",
       "      <td>Poole Matthew</td>\n",
       "      <td>MatthewPoole@gustr.com</td>\n",
       "      <td>ORG_INVITE</td>\n",
       "      <td>2014-03-31 03:45:04</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>1</td>\n",
       "      <td>316.0</td>\n",
       "      <td>1</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>3</td>\n",
       "      <td>2013-03-19 23:14:52</td>\n",
       "      <td>Bottrill Mitchell</td>\n",
       "      <td>MitchellBottrill@gustr.com</td>\n",
       "      <td>ORG_INVITE</td>\n",
       "      <td>2013-03-19 23:14:52</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>94</td>\n",
       "      <td>1525.0</td>\n",
       "      <td>0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>3</th>\n",
       "      <td>4</td>\n",
       "      <td>2013-05-21 08:09:28</td>\n",
       "      <td>Clausen Nicklas</td>\n",
       "      <td>NicklasSClausen@yahoo.com</td>\n",
       "      <td>GUEST_INVITE</td>\n",
       "      <td>2013-05-22 08:09:28</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>1</td>\n",
       "      <td>5151.0</td>\n",
       "      <td>0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>4</th>\n",
       "      <td>5</td>\n",
       "      <td>2013-01-17 10:14:20</td>\n",
       "      <td>Raw Grace</td>\n",
       "      <td>GraceRaw@yahoo.com</td>\n",
       "      <td>GUEST_INVITE</td>\n",
       "      <td>2013-01-22 10:14:20</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>193</td>\n",
       "      <td>5240.0</td>\n",
       "      <td>0</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "   user_id       creation_time               name                       email  \\\n",
       "0        1 2014-04-22 03:53:30     Clausen August    AugustCClausen@yahoo.com   \n",
       "1        2 2013-11-15 03:45:04      Poole Matthew      MatthewPoole@gustr.com   \n",
       "2        3 2013-03-19 23:14:52  Bottrill Mitchell  MitchellBottrill@gustr.com   \n",
       "3        4 2013-05-21 08:09:28    Clausen Nicklas   NicklasSClausen@yahoo.com   \n",
       "4        5 2013-01-17 10:14:20          Raw Grace          GraceRaw@yahoo.com   \n",
       "\n",
       "  creation_source last_session_creation_time  opted_in_to_mailing_list  \\\n",
       "0    GUEST_INVITE        2014-04-22 03:53:30                         1   \n",
       "1      ORG_INVITE        2014-03-31 03:45:04                         0   \n",
       "2      ORG_INVITE        2013-03-19 23:14:52                         0   \n",
       "3    GUEST_INVITE        2013-05-22 08:09:28                         0   \n",
       "4    GUEST_INVITE        2013-01-22 10:14:20                         0   \n",
       "\n",
       "   enabled_for_marketing_drip  org_id  invited_by_user_id  adopted  \n",
       "0                           0      11             10803.0        0  \n",
       "1                           0       1               316.0        1  \n",
       "2                           0      94              1525.0        0  \n",
       "3                           0       1              5151.0        0  \n",
       "4                           0     193              5240.0        0  "
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "df.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<class 'pandas.core.frame.DataFrame'>\n",
      "RangeIndex: 12000 entries, 0 to 11999\n",
      "Data columns (total 11 columns):\n",
      " #   Column                      Non-Null Count  Dtype         \n",
      "---  ------                      --------------  -----         \n",
      " 0   user_id                     12000 non-null  int64         \n",
      " 1   creation_time               12000 non-null  datetime64[ns]\n",
      " 2   name                        12000 non-null  object        \n",
      " 3   email                       12000 non-null  object        \n",
      " 4   creation_source             12000 non-null  object        \n",
      " 5   last_session_creation_time  8823 non-null   datetime64[ns]\n",
      " 6   opted_in_to_mailing_list    12000 non-null  int64         \n",
      " 7   enabled_for_marketing_drip  12000 non-null  int64         \n",
      " 8   org_id                      12000 non-null  int64         \n",
      " 9   invited_by_user_id          6417 non-null   float64       \n",
      " 10  adopted                     12000 non-null  int64         \n",
      "dtypes: datetime64[ns](2), float64(1), int64(5), object(3)\n",
      "memory usage: 1.0+ MB\n"
     ]
    }
   ],
   "source": [
    "df.info()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that the feature `last_session_creation_time` 3,177 missing values. I will assume the missing values indicate that the user has not logged again after the account was created, so I will impute the values by replacing the missing values with the values in `creation_time`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [],
   "source": [
    "df[\"last_session_creation_time\"].fillna(df[\"creation_time\"], inplace=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Create a column representing the difference in time between creation and last login\n",
    "df[\"usage_lifespan\"] = df[\"last_session_creation_time\"] - df[\"creation_time\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I will need to drop the `name` and `email` columns because applying one hot encoding to the dataframe will result in each unique name and email having it's own column, creating too many columns. Since I don't think these two columns will have a significant effect on the model, I'm dropping them."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [],
   "source": [
    "df = df.drop([\"name\", \"email\"], axis=1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {
    "scrolled": false
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>user_id</th>\n",
       "      <th>creation_time</th>\n",
       "      <th>last_session_creation_time</th>\n",
       "      <th>opted_in_to_mailing_list</th>\n",
       "      <th>enabled_for_marketing_drip</th>\n",
       "      <th>org_id</th>\n",
       "      <th>invited_by_user_id</th>\n",
       "      <th>adopted</th>\n",
       "      <th>usage_lifespan</th>\n",
       "      <th>creation_source_GUEST_INVITE</th>\n",
       "      <th>creation_source_ORG_INVITE</th>\n",
       "      <th>creation_source_PERSONAL_PROJECTS</th>\n",
       "      <th>creation_source_SIGNUP</th>\n",
       "      <th>creation_source_SIGNUP_GOOGLE_AUTH</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>1</td>\n",
       "      <td>2014-04-22 03:53:30</td>\n",
       "      <td>2014-04-22 03:53:30</td>\n",
       "      <td>1</td>\n",
       "      <td>0</td>\n",
       "      <td>11</td>\n",
       "      <td>10803.0</td>\n",
       "      <td>0</td>\n",
       "      <td>0 days</td>\n",
       "      <td>1</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>2</td>\n",
       "      <td>2013-11-15 03:45:04</td>\n",
       "      <td>2014-03-31 03:45:04</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>1</td>\n",
       "      <td>316.0</td>\n",
       "      <td>1</td>\n",
       "      <td>136 days</td>\n",
       "      <td>0</td>\n",
       "      <td>1</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>3</td>\n",
       "      <td>2013-03-19 23:14:52</td>\n",
       "      <td>2013-03-19 23:14:52</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>94</td>\n",
       "      <td>1525.0</td>\n",
       "      <td>0</td>\n",
       "      <td>0 days</td>\n",
       "      <td>0</td>\n",
       "      <td>1</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>3</th>\n",
       "      <td>4</td>\n",
       "      <td>2013-05-21 08:09:28</td>\n",
       "      <td>2013-05-22 08:09:28</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>1</td>\n",
       "      <td>5151.0</td>\n",
       "      <td>0</td>\n",
       "      <td>1 days</td>\n",
       "      <td>1</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>4</th>\n",
       "      <td>5</td>\n",
       "      <td>2013-01-17 10:14:20</td>\n",
       "      <td>2013-01-22 10:14:20</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>193</td>\n",
       "      <td>5240.0</td>\n",
       "      <td>0</td>\n",
       "      <td>5 days</td>\n",
       "      <td>1</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "   user_id       creation_time last_session_creation_time  \\\n",
       "0        1 2014-04-22 03:53:30        2014-04-22 03:53:30   \n",
       "1        2 2013-11-15 03:45:04        2014-03-31 03:45:04   \n",
       "2        3 2013-03-19 23:14:52        2013-03-19 23:14:52   \n",
       "3        4 2013-05-21 08:09:28        2013-05-22 08:09:28   \n",
       "4        5 2013-01-17 10:14:20        2013-01-22 10:14:20   \n",
       "\n",
       "   opted_in_to_mailing_list  enabled_for_marketing_drip  org_id  \\\n",
       "0                         1                           0      11   \n",
       "1                         0                           0       1   \n",
       "2                         0                           0      94   \n",
       "3                         0                           0       1   \n",
       "4                         0                           0     193   \n",
       "\n",
       "   invited_by_user_id  adopted usage_lifespan  creation_source_GUEST_INVITE  \\\n",
       "0             10803.0        0         0 days                             1   \n",
       "1               316.0        1       136 days                             0   \n",
       "2              1525.0        0         0 days                             0   \n",
       "3              5151.0        0         1 days                             1   \n",
       "4              5240.0        0         5 days                             1   \n",
       "\n",
       "   creation_source_ORG_INVITE  creation_source_PERSONAL_PROJECTS  \\\n",
       "0                           0                                  0   \n",
       "1                           1                                  0   \n",
       "2                           1                                  0   \n",
       "3                           0                                  0   \n",
       "4                           0                                  0   \n",
       "\n",
       "   creation_source_SIGNUP  creation_source_SIGNUP_GOOGLE_AUTH  \n",
       "0                       0                                   0  \n",
       "1                       0                                   0  \n",
       "2                       0                                   0  \n",
       "3                       0                                   0  \n",
       "4                       0                                   0  "
      ]
     },
     "execution_count": 18,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "df = pd.get_dummies(df)\n",
    "df.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.1335"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "df[\"adopted\"].mean()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I calculated the mean of `adopted` to get a percentage of adoption. We can see that only 13% of users have adopted the product.  \n",
    "We will investigate which features may help increase that percentage."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [],
   "source": [
    "def fit_pred_plot(X, y, model, parameters, test_size=0.2, random_state=47):\n",
    "    r\"\"\" Fit the model on the chosen hyperparameters. \n",
    "    Predict values. \n",
    "    Print used hyperparameters, accuracy score, confusion matrix, classification report, and feature importance table.\n",
    "    Plot ROC curve , or precision recall curve.\"\"\"\n",
    "    \n",
    "    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size, random_state=random_state)\n",
    "    \n",
    "    print(\"training sample size: {}\".format(len(X_train)))\n",
    "    print(\"testing sample size: {}\".format(len(X_test)))\n",
    "    \n",
    "    model = model(parameters)\n",
    "    \n",
    "    model.fit(X_train, y_train)\n",
    "    \n",
    "    y_pred = model.predict(X_test)\n",
    "    \n",
    "    # Print the optimal parameters and best score\n",
    "    print(\"Hyperparameters: {}\".format(parameters))\n",
    "    print(\"Accuracy Score: {}\".format(metrics.accuracy_score(y_test, y_pred)))\n",
    "    print(\"Confusion Matrix: {}\".format(metrics.confusion_matrix(y_test, y_pred))) # TP, TN, FP, FN\n",
    "    print(\"Classification Report: {}\".format(classification_report(y_test, y_pred)))\n",
    "    feature_importance = pd.DataFrame(model.feature_importances_, index=X_train.columns, columns=['importance']).sort_values('importance', ascending=False)\n",
    "    print(\"feature importance: {}\".format(feature_importance))\n",
    "    \n",
    "    # plot goes here"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [],
   "source": [
    "# split the dataset into training & testing data\n",
    "target_feature = \"adopted\"\n",
    "X = df.drop(target_feature, axis=1)\n",
    "y = df[[target_feature]]\n",
    "\n",
    "# train_test_split outside of function to use in hyperparameter for-loop below\n",
    "X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=47)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Logistic Regression\n",
    "# hyperparameter for-loop\n",
    "c_param = [0.01, 0.1, 1, 10, 100]\n",
    "\n",
    "logreg_acc_scores = []\n",
    "for c in c_param:\n",
    "    logreg = LogisticRegression(C=i, penalty=\"l2\", random_state=47)\n",
    "    logreg.fit(X_train, y_train)\n",
    "    y_pred = logreg.predict(X_test)\n",
    "    \n",
    "    accuracy = accuracy_score(y_test, y_pred)\n",
    "    logreg_acc_scores.append(accuracy)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fit_pred_plot(X, y, LogisticRegression(), parameters=c, test_size=0.2, random_state=47)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Random Forest\n",
    "# hyperparameter for-loop\n",
    "n_estimator = [50, 100, 200]\n",
    "max_depth = [None, 3]\n",
    "criterion = [\"gini\", \"entropy\"]\n",
    "\n",
    "rf_acc_scores = []\n",
    "for c in c_param:\n",
    "    rf = RandomForestClassifier(n_estimator=n_estimator, max_depth=max_depth, \n",
    "                                criterion=criterion, random_state=47\n",
    "                               )\n",
    "    rf.fit(X_train, y_train)\n",
    "    y_pred = rf.predict(X_test)\n",
    "    \n",
    "    accuracy = accuracy_score(y_test, y_pred)\n",
    "    rf_acc_scores.append(accuracy)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fit_pred_plot(X, y, RandomForestClassifier(), parameters=, test_size=0.2, random_state=47)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# K-Nearest Neighbor (KNN)\n",
    "# hyperparameter for-loop\n",
    "N = [1, 3, 5, 7, 11]\n",
    "\n",
    "knn_acc_scores = []\n",
    "for n in N:\n",
    "    knn = KNeighborsClassifier(n_neighbors=n, random_state=47)\n",
    "    knn.fit(X_train, y_train)\n",
    "    y_pred = knn.predict(X_test)\n",
    "    \n",
    "    accuracy = accuracy_score(y_test, y_pred)\n",
    "    knn_acc_scores.append(accuracy)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fit_pred_plot(X, y, KNeighborsClassifier(), parameters=, test_size=0.2, random_state=47)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "check the stats of creation_source.. which has more numbers.  \n",
    "Maybe put in more resources into that source.  \n",
    "Also do the same inspection with org_id."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# both files are csv files so I used pandas read_csv to open them as dataframes. \n",
    "\n",
    "# encoding: I first read the csv without specifying the encoding type. \n",
    "# In doing so, I got a UnicodeDecodeError message: 'utf-8' codec can't decode byte 0xe6 in position 11: invalid continuation byte.\n",
    "# This led me to consider an encoding method other than utf-8. \n",
    "\n",
    "# For the users table, the feature \"creation_time\" had an object datatype. \n",
    "# I used parse_dates to parse the dates in the column and load it as a datetime feature. \n",
    "\n",
    "# For the user engagement table, I parsed the dates for the time_stamp feature. \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
